import React, { Component } from 'react';

class Searches extends Component {
	handleKeyPress = event => {
		if (event.key === 'Enter') {
			this.props.click(event);
		}
	};

	render() {
		return (
			<div>
				<div className='container-search'>
					<input onKeyPress={this.handleKeyPress} type='text'></input>
					<button className='btn-search'>
						<img onClick={this.props.click} className='test' src='./search1.png' alt='search'></img>
					</button>
				</div>
			</div>
		);
	}
}

export default Searches;


