import React from 'react';

const ItemCrypto = props => (
	<div  className='coin'>
		<div>
			<img src={props.image} className='img' alt={props}></img>
		</div>
		
		<div>{props.name}</div>
		<div className={props.color}>{props.price + '/USD'}</div>
	</div>
);

export default ItemCrypto;
