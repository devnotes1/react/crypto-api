import React, { Component } from 'react';
import './App.css';
import Searches from './Search';
import ItemCrypto from './ItemCrypto';
import SingleCrypto from './SingleCrypto';

const options = {
	method: 'GET',
	headers: { accept: 'application/json', 'x-cg-demo-api-key': 'CG-Wcsj9F9y8k9W8kAvnRD4qjfc' },
};

class App extends Component {
	state = {
		crypto: [],
		isLoaded: false,
		single: null,
		cross: null,
		chosenCoin: {},
	};

	componentDidMount() {
		fetch('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd', options)
			.then(response => response.json())
			.then(data => {
				this.setState({
					crypto: data,
					isLoaded: true,
					single: document.querySelector('.single'),
					cross: document.querySelector('.cross'),
				});
			});

	
		document.addEventListener('keydown', this.handleKeyDown);
	}

	
	componentWillUnmount() {
		document.removeEventListener('keydown', this.handleKeyDown);
	}


	handleKeyDown = (e) => {
		if (e.key === 'Escape') {
			this.clickCross();
		}
	};

	searchCrypto = e => {
		e.preventDefault();
		const input = document.querySelector('input');
		const inputValue = input.value;
		const { crypto, cross, single } = this.state;

		crypto.forEach(coin => {
			if (coin.name.toLowerCase() === inputValue.toLowerCase()) {
				console.log(coin);
				single.classList.add('open');
				cross.classList.add('show');

				this.setState({
					chosenCoin: coin,
				});
			}
		});
	};

	clickCross = () => {
		const { cross, single } = this.state;
		cross.classList.remove('show');
		single.classList.remove('open');
		this.setState({ selectedCrypto: null });
	};

	render() {
		const items = this.state.crypto
			.slice(0, 5)
			.map(item => (
				<ItemCrypto
					color={item.market_cap_change_percentage_24h > 0 ? 'green' : 'red'}
					key={item.id}
					name={item.name}
					price={item.current_price}
					image={item.image}
				/>
			));

		return (
			<div className='container'>
				<div className='wrapper'>
					<div className='home'>
						<Searches  click={this.searchCrypto} />
						<div>
							<h1>Sprawdź swoje krypto</h1>
						</div>
						<div>
							<div className='container__crypto'>{this.state.isLoaded ? items : 'Wczytuję dane'}</div>
						</div>
					</div>

					<div className='single'>
						<SingleCrypto coin={this.state.chosenCoin} />
					</div>
				</div>

				<div className='cross'>
					<img src='./cross.png' alt='cross'  onClick={this.clickCross}></img>
				</div>
			</div>
		);
	}
}

export default App;