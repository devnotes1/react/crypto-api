import React from 'react';

// Komponent SingleCrypto otrzymuje dane jako props
const SingleCrypto = data => {
	const coin = data.coin;
	// Jeśli brak danych, nie wyświetla nic
	if (!coin) return () => console.log('nie działa');

	return (
		<div>
			<div className='price'>
				<div className='price__number'>{coin.current_price}/USD</div>
				<div className='price__change'>{coin.price_change_percentage_24h}%</div>
			</div>

			<div className='single__image'>
				<img src={coin.image} className='single__img'></img>
			</div>

			<div>
				{/* Iteracja po wszystkich kluczach i wartościach w obiekcie data */}
				{Object.entries(data.coin)
					.slice(0, 14)
					.map(([key, value]) => {
						// Zmienna przechowująca JSX do renderowania
						let content;

						// Pełna wersja instrukcji warunkowej if-else
						if (key === 'image') return;
						else {
							content = (
								<>
									<strong>{key}: </strong>
									<span>{value}</span>
								</>
							);
						}

						// Zwraca odpowiedni JSX w zależności od klucza
						return (
							<div key={key} className='item'>
								{content}
							</div>
						);
					})}
			</div>
		</div>
	);
};

export default SingleCrypto;
